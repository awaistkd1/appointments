<link rel="stylesheet" type="text/css" href="<?= asset_url('/assets/ext/jquery-fullcalendar/fullcalendar.css') ?>">
<link rel="stylesheet" type="text/css" href="<?= asset_url('/assets/ext/auto-complete/jquery-ui.css') ?>">
<link rel="stylesheet" type="text/css" href="<?= base_url('/assets/ext/jquery-fullcalendar/scheduler.css') ?>">
<style>
    #myDIV{
    position: fixed; /* Sit on top of the page content */
    display: none; /* Hidden by default */
    width: 400px; /* Full width (cover the whole page) */
    height: 450px; /* Full height (cover the whole page) */
    top: 110px; 
    left: 800px;
    right: 0;
    bottom: 0;
    z-index: 2; /* Specify a stack order in case you're using a different order for other elements */
    cursor: pointer; /* Add a pointer on hover */
    position: absolute;
    background:White;
    }
    #myDIV2{
    position: fixed; /* Sit on top of the page content */
    display: none; /* Hidden by default */
    width: 400px; /* Full width (cover the whole page) */
    height: 500px; /* Full height (cover the whole page) */
    top: 60px; 
    left: 800px;
    right: 0;
    bottom: 0;
    z-index: 3; /* Specify a stack order in case you're using a different order for other elements */
    cursor: pointer; /* Add a pointer on hover */
    position: absolute;
    background:White;
    }
    .ui-autocomplete {
    padding: 0;
    list-style: none;
    background-color: #fff;
    width: 218px;
    border: 1px solid #B0BECA;
    max-height: 350px;
    overflow-x: hidden;
    z-index: 9999;
    }
    .ui-autocomplete .ui-menu-item {
    border-top: 1px solid #B0BECA;
    display: block;
    padding: 4px 6px;
    color: #353D44;
    cursor: pointer;
    }
    .ui-autocomplete .ui-menu-item:first-child {
    border-top: none;
    }
    .ui-autocomplete .ui-menu-item.ui-state-focus {
    background-color: #D5E5F4;
    color: #161A1C;
    }

    #manage-appointment .modal-dialog{
        width: 95%;
        max-width: 1400px;
    }
</style>
<script>window.from_select_event = false;</script>
<script src="<?= asset_url('assets/ext/mustache/mustache.min.js') ?>"></script>
<script src="<?= asset_url('assets/ext/moment/moment.min.js') ?>"></script>
<script src="<?= asset_url('assets/ext/jquery-fullcalendar/fullcalendar.js') ?>"></script>
<script src="<?= base_url('assets/ext/jquery-fullcalendar/scheduler.js') ?>"></script>
<script src="<?= asset_url('assets/js/fullcalendar-resource.js') ?>"></script>
<!-- jQuery Auto Complete -->
<script src="<?= asset_url('assets/ext/auto-complete/jquery-ui.min.js') ?>"></script>
<script src="<?= asset_url('assets/ext/auto-complete/jquery.select-to-autocomplete.js') ?>"></script>
<script src="<?= asset_url('assets/ext/jquery-ui/jquery-ui-timepicker-addon.js') ?>"></script>
<script src="<?= asset_url('assets/js/fullcalendar-resource.js') ?>"></script>
<script src="<?= asset_url('assets/js/backend_calendar.js') ?>"></script>
<script src="<?= asset_url('assets/js/backend_calendar_default_view.js') ?>"></script>
<script src="<?= asset_url('assets/js/backend_calendar_table_view.js') ?>"></script>
<script src="<?= asset_url('assets/js/backend_calendar_resource_view.js') ?>"></script>
<script src="<?= asset_url('assets/js/backend_calendar_google_sync.js') ?>"></script>
<script src="<?= asset_url('assets/js/backend_calendar_appointments_modal.js') ?>"></script>
<script src="<?= asset_url('assets/js/backend_calendar_appointments_modal_activities.js') ?>"></script>
<script src="<?= asset_url('assets/js/backend_calendar_unavailabilities_modal.js') ?>"></script>
<script src="<?= asset_url('assets/js/backend_calendar_api.js') ?>"></script>
<script>
    var GlobalVariables = {
        'csrfToken'             : <?= json_encode($this->security->get_csrf_hash()) ?>,
        'availableProviders'    : <?= json_encode($available_providers) ?>,
        'availableServices'     : <?= json_encode($available_services) ?>,
        'baseUrl'               : <?= json_encode($base_url) ?>,
        'bookAdvanceTimeout'    : <?= $book_advance_timeout ?>,
        'dateFormat'            : <?= json_encode($date_format) ?>,
        'editAppointment'       : <?= json_encode($edit_appointment) ?>,
        'customers'             : <?= json_encode($customers) ?>,
        'secretaryProviders'    : <?= json_encode($secretary_providers) ?>,
        'calendarView'          : <?= json_encode($calendar_view) ?>,
        'statusColorMapping'    : <?= json_encode($status_color_mapping) ?>,
        'providerCategories'    : <?= json_encode($provider_categories) ?>,
        'user'                  : {
            'id'        : <?= $user_id ?>,
            'email'     : <?= json_encode($user_email) ?>,
            'role_slug' : <?= json_encode($role_slug) ?>,
            'privileges': <?= json_encode($privileges) ?>
        }
    };
    
    $(document).ready(function() {
        BackendCalendar.initialize(GlobalVariables.calendarView);
    });
</script>
<div id="calendar-page" class="container-fluid">
    <div id="calendar-toolbar">
        <div id="calendar-filter" class="form-inline col-xs-12 col-sm-5">
            <div class="form-group">
                <label for="select-filter-item"><?= lang('display_calendar') ?></label>
                <select id="select-filter-item" class="form-control" title="<?= lang('select_filter_item_hint') ?>">
                </select>
            </div>
        </div>
        <div id="calendar-actions" class="col-xs-12 col-sm-7">
            <?php if (($role_slug == DB_SLUG_ADMIN || $role_slug == DB_SLUG_PROVIDER)
                && Config::GOOGLE_SYNC_FEATURE == TRUE): ?>
            <button id="google-sync" class="btn btn-primary"
                title="<?= lang('trigger_google_sync_hint') ?>">
            <span class="glyphicon glyphicon-refresh"></span>
            <span><?= lang('synchronize') ?></span>
            </button>
            <button id="enable-sync" class="btn btn-default" data-toggle="button"
                title="<?= lang('enable_appointment_sync_hint') ?>">
            <span class="glyphicon glyphicon-calendar"></span>
            <span><?= lang('enable_sync') ?></span>
            </button>
            <?php endif ?>
            <?php if ($privileges[PRIV_APPOINTMENTS]['add'] == TRUE): ?>
            <button id="insert-appointment" class="btn btn-default" title="<?= lang('new_appointment_hint') ?>">
            <span class="glyphicon glyphicon-plus"></span>
            <?= lang('appointment') ?>
            </button>
            <button id="insert-unavailable" class="btn btn-default" title="<?= lang('unavailable_periods_hint') ?>">
            <span class="glyphicon glyphicon-plus"></span>
            <?= lang('unavailable') ?>
            </button>
            <?php endif ?>
            <button id="reload-appointments" class="btn btn-default" title="<?= lang('reload_appointments_hint') ?>">
            <span class="glyphicon glyphicon-repeat"></span>
            <?= lang('reload') ?>
            </button>
            <button id="toggle-fullscreen" class="btn btn-default">
            <span class="glyphicon glyphicon-fullscreen"></span>
            </button>
        </div>
    </div>
    <div id="calendar">
        <!-- Dynamically Generated Content -->
    </div>
</div>
<!-- MANAGE APPOINTMENT MODAL -->
<div id="manage-appointment" class="modal fade" data-keyboard="true" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title"><?= lang('edit_appointment_title') ?></h3>
            </div>
            <div class="modal-body">
                <div class="modal-message alert hidden"></div>
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#appointment-tab" aria-controls="home" role="tab" data-toggle="tab"><?= lang('appointment') ?></a></li>
                    <li role="presentation"><a href="#activity-tab" aria-controls="activity" role="tab" data-toggle="tab"><?= lang('activity') ?></a></li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" style="min-height: 500px" id="appointment-tab">
                        <form class="form-horizontal">

                            <div class="row">
                                <div class="col col-sm-8 form-column">

                                    <fieldset id="appointment-container" style="max-height: 564px; overflow: auto;">
                                        <legend style="padding-left: 10px;"><?= lang('appointment_details_title') ?></legend>
                                    </fieldset>
                                    <button id="add-new-appointment" class="btn btn-default btn-xs">Add</button>
                                    
                                </div>
                                <div class="col col-sm-4">
                                    
                                    <div id="cutomers-list-tab">
                                        <h4>
                                            <?= lang('customer_details_title') ?>
                                            <button id="new-customer" class="btn btn-default btn-xs"
                                                title="<?= lang('clear_fields_add_existing_customer_hint') ?>"
                                                type="button"><?= lang('new') ?>
                                            </button>
                                            <button id="select-customer" class="btn btn-primary btn-xs"
                                                title="<?= lang('pick_existing_customer_hint') ?>"
                                                type="button"><?= lang('select') ?>
                                            </button>
                                        </h4>
                                        <input id="filter-existing-customers"
                                            placeholder="<?= lang('type_to_filter_customers') ?>"  class="input-sm form-control">

                                        <div id="existing-customers-list"></div>
                                    </div>
                                    
                                    <input id="customer-id" type="hidden" />

                                    <div id="customer-info" style="display: none;">
                                    </div>

                                </div>
                            </div>

                            <input id="appointment-id" type="hidden">
                            <input id="appointment-status" type="hidden">
                            <input id="appointment-status-original" type="hidden">
                            <input id="appointment-confirmation-status" type="hidden">
                            <input id="appointment-confirmation-status-original" type="hidden">

                        </form>


                        <div class="row" style="margin-top: 20px;">
                            <div class="col col-sm-6">
                                <div class="form-group">
                                    <label for="appointment-notes" class="control-label col-sm-3"><?= lang('notes') ?></label>
                                    <div class="col-sm-7">
                                        <textarea style="width: 250px;" id="appointment-notes" class="form-control" rows="2"></textarea>
                                    </div>
                                </div>                        
                            </div>
                            <div class="col col-sm-6">
                                <div class="form-group">
                                    <button id="confirmed-button" class="btn btn-default btn-xs btn-status" data-status="confirmed" title="<?= lang('confirmed') ?>" type = "button">
                                        <?= lang('confirmed') ?>
                                        <i class="far fa-thumbs-up"></i>
                                        <!--                                                <img src="--><?//= base_url('assets/img/confirmed.png') ?><!--">-->
                                    </button>
                                    <button id="not-confirmed-button" class="btn btn-default btn-xs btn-status" data-status="not-confirmed" title="<?= lang('not_confirmed') ?>" type = "button">
                                        <?= lang('not_confirmed') ?>
                                        <i class="far fa-thumbs-down"></i>
                                        <!--                                                <img src="--><?//= base_url('assets/img/not_confirmed.png') ?><!--">-->
                                    </button>
                                </div>
                                <button id="arrive-button" class="btn btn-default btn-xs btn-status" data-status="arrive" title="<?= lang('arrive') ?>" type = "button">
                                <?= lang('arrive') ?>
                                </button>
                                <button id="start-button" class="btn btn-default btn-xs btn-status" data-status="start" title="<?= lang('start') ?>" type = "button">
                                <?= lang('start') ?>
                                </button>
                                <button id="complete-button" class="btn btn-default btn-xs btn-status" data-status="complete" title="<?= lang('complete') ?>" type = "button">
                                <?= lang('complete') ?>
                                </button>
                                <button id="checkout-button" class="btn btn-default btn-xs btn-status" data-status="checkout" title="<?= lang('checkout') ?>" type = "button">
                                <?= lang('checkout') ?>
                                </button>
                                <button id="no-show-button" class="btn btn-default btn-xs btn-status" data-status="no-show" title="<?= lang('no-show') ?>" type = "button">
                                <?= lang('no-show') ?>
                                </button>                        
                            </div>
                        </div>



                    </div>
                    <div role="tabpanel" class="tab-pane" id="activity-tab">
                        <div class="activity-container">
                            <!-- Dynamic content -->
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="modal-footer">
                
                <button id="save-appointment" class="btn btn-primary"><?= lang('save') ?></button>
                <button id="cancel-appointment" class="btn btn-default" data-dismiss="modal"><?= lang('cancel') ?></button>
            </div>
        </div>
    </div>
</div>
<!-- MANAGE UNAVAILABLE MODAL -->
<div id="manage-unavailable" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title"><?= lang('new_unavailable_title') ?></h3>
            </div>
            <div class="modal-body">
                <div class="modal-message alert hidden"></div>
                <form>
                    <fieldset>
                        <input id="unavailable-id" type="hidden">
                        <div class="form-group">
                            <label for="unavailable-provider" class="control-label"><?= lang('provider') ?></label>
                            <select id="unavailable-provider" class="form-control"></select>
                        </div>
                        <div class="form-group">
                            <label for="unavailable-start" class="control-label"><?= lang('start') ?></label>
                            <input id="unavailable-start" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="unavailable-end" class="control-label"><?= lang('end') ?></label>
                            <input id="unavailable-end" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="unavailable-notes" class="control-label"><?= lang('notes') ?></label>
                            <textarea id="unavailable-notes" rows="3" class="form-control"></textarea>
                        </div>
                    </fieldset>
                </form>
            </div>
            <div class="modal-footer">
                <button id="save-unavailable" class="btn btn-primary"><?= lang('save') ?></button>
                <button id="cancel-unavailable" class="btn btn-default" data-dismiss="modal"><?= lang('cancel') ?></button>
            </div>
        </div>
    </div>
</div>
<!-- SELECT GOOGLE CALENDAR MODAL -->
<div id="select-google-calendar" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title"><?= lang('select_google_calendar') ?></h3>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="google-calendar" class="control-label"><?= lang('select_google_calendar_prompt') ?></label>
                    <select id="google-calendar" class="form-control"></select>
                </div>
            </div>
            <div class="modal-footer">
                <button id="select-calendar" class="btn btn-primary"><?= lang('select') ?></button>
                <button id="close-calendar" class="btn btn-default" data-dismiss="modal"><?= lang('close') ?></button>
            </div>
        </div>
    </div>
</div>

<script id="customer-template" type="x-tmpl-mustache">

    <div class="row">
        <div class="col col-12">
            <h4 style="margin:0;">{{customer_name}}</h4>
            <p style="margin-bottom: 0;">{{phone_number}}</p>
        </div>
    </div>

    <div class="row">
        <div class="col col-12">
            <span class="badge noshow-badge">{{no_show_count}} NO SHOW</span>
        </div>
    </div>

    <div class="row">
        <div class="col col-12">
            <div class="booking-count-total">
                <h4>{{total_bookings}}</h4>
                <p>Total Bookings</p>             
            </div>
        </div>
    </div>                                            

    <div class="row">
        <div class="col col-12">
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#all-appointment-tab">APPOINTMENTS</a></li>
              <li><a data-toggle="tab" href="#customer-info-tab">INFO</a></li>
            </ul>
            <div class="tab-content">
                <div id="all-appointment-tab" class="tab-pane fade in active">
                    {{#appointments}}
                    <div class="row appointment-row">
                        <div class="col col-sm-3">
                            <div class="date-time">
                                <p style="font-size: 14px"><b>{{month}}</b></p>
                                <p style="font-size: 10px">{{time}}</p>
                            </div>
                        </div>
                        <div class="col col-sm-9 list-col">
                            <span class="total-cost">{{total}} JD</span>
                            <ul>
                                {{#data}}
                                <li>
                                    <h5>{{service.name}}</h5>
                                    <p>{{service.duration}} min with {{#request_status}}<span class="glyphicon glyphicon-heart"></span>{{/request_status}} {{provider.first_name}}</p>
                                </li>
                                {{/data}}                                                                                                                            
                            </ul>
                        </div>
                    </div>
                    {{/appointments}}
                </div>
                <div id="customer-info-tab" class="tab-pane fade">

                    <!-- Info Here -->
                    <div class="form-group">
                        <label for="first-name" class="control-label col-sm-4"><?= lang('name') ?> *</label>
                        <div class="col-sm-8">
                            <input id="first-name"  style="height: 30px;" value="{{first_name}}" class="required form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="last-name" class="control-label col-sm-4"><?= lang('last_name') ?> *</label>
                        <div class="col-sm-8">
                            <input id="last-name" style="height: 30px;" value="{{last_name}}" class="required form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="phone-number" class="control-label col-sm-4"><?= lang('phone_number') ?> *</label>
                        <div class="col-sm-8">
                            <input id="phone-number" style="height: 30px;" value="{{phone_number}}" class="required form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email" class="control-label col-sm-4"><?= lang('email') ?></label>
                        <div class="col-sm-8">
                            <input id="email" style="height: 30px;" value="{{email}}" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="address" class="control-label col-sm-4"><?= lang('address') ?></label>
                        <div class="col-sm-8">
                            <input id="address" style="height: 30px;" value="{{address}}" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="city" class="control-label col-sm-4"><?= lang('city') ?></label>
                        <div class="col-sm-8">
                            <input id="city" style="height: 30px;" value="{{city}}" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="customer-notes" class="control-label col-sm-4"><?= lang('notes') ?></label>
                        <div class="col-sm-8">
                            <textarea id="customer-notes" rows="2" class="form-control">{{notes}}</textarea>
                        </div>
                    </div>
                </div>
            </div>     
        </div>                                
    </div>

    <script>
        var no_show_count = {{no_show_count}};
        if(no_show_count == 0){
            $('.noshow-badge').css('background-color','green');
        }else if( (no_show_count == 1) || (no_show_count == 2) ){
            $('.noshow-badge').css('background-color','orange');
        }else{
            $('.noshow-badge').css('background-color','red');
        }
    </script>
</script>


<script id="appointment-template" type="x-tmpl-mustache">
    <div class="row sub-appointment">

        <span class="close-sub-appointment">&times;</span>
        <input id="appointment-request-status-{{id}}" class="appointment-request-status" type="hidden" value="0">
        <div class="col-xs-12 col-sm-7">
            
            <div class="form-group">
                <label for="select-provider-{{id}}" class="control-label col-sm-3">
                    <?= lang('provider') ?> *   &nbsp; 
                    <button id="request-button-{{id}}" class="btn btn-default btn-xs btn-status" data-status="request" title="<?= lang('requested') ?>" type = "button">
                        <span class="glyphicon glyphicon-heart-empty"></span>
                    </button>
                </label>
                <div class="col-sm-9">
                    <select id="select-provider-{{id}}" class="select-provider required form-control">

                    </select>
                </div>
            </div>            

            <div class="form-group">
                <label for="select-service-{{id}}" class="control-label col-sm-3"><?= lang('service') ?> *</label>
                <div class="col-sm-9" >
                    <select id="select-service-{{id}}" class="select-service required form-control" autocorrect="off"  autocomplete="off">
                    </select>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-5">
            <div class="form-group">
                <label for="start-datetime" class="control-label col-sm-5"><?= lang('start_date_time') ?></label>
                <div class="col-sm-7">
                    <input class="start-datetime required form-control" id="start-datetime-{{id}}">
                </div>
            </div>
            <div class="form-group">
                <label for="end-datetime" class="control-label col-sm-5"><?= lang('end_date_time') ?></label>
                <div class="col-sm-7">
                    <input class="end-datetime required form-control" id="end-datetime-{{id}}">
                </div>
            </div>
        </div>
    </div>

    <script>

        $.each(GlobalVariables.availableProviders, function (index, provider) {
            
            var optionHtml = '<option value="' + provider.id + '">'
                    + provider.first_name + ' ' + provider.last_name +'</option>';

            $('#select-provider-{{id}}').append(optionHtml);

        });


        $('#select-provider-{{id}}').selectToAutocomplete();

        //   
        var $dialog = $('#manage-appointment');


        var dateFormat;

        switch (GlobalVariables.dateFormat) {
            case 'DMY':
                dateFormat = 'dd/mm/yy';
                break;
            case 'MDY':
                dateFormat = 'mm/dd/yy';
                break;
            case 'YMD':
                dateFormat = 'yy/mm/dd';
                break;
            default:
                throw new Error('Invalid GlobalVariables.dateFormat value.');
        }

        $dialog.find('#start-datetime-{{id}}').datetimepicker({
            dateFormat: dateFormat,
            timeFormat: 'h:mm TT',

            // Translation
            dayNames: [EALang.sunday, EALang.monday, EALang.tuesday, EALang.wednesday,
                EALang.thursday, EALang.friday, EALang.saturday],
            dayNamesShort: [EALang.sunday.substr(0, 3), EALang.monday.substr(0, 3),
                EALang.tuesday.substr(0, 3), EALang.wednesday.substr(0, 3),
                EALang.thursday.substr(0, 3), EALang.friday.substr(0, 3),
                EALang.saturday.substr(0, 3)],
            dayNamesMin: [EALang.sunday.substr(0, 2), EALang.monday.substr(0, 2),
                EALang.tuesday.substr(0, 2), EALang.wednesday.substr(0, 2),
                EALang.thursday.substr(0, 2), EALang.friday.substr(0, 2),
                EALang.saturday.substr(0, 2)],
            monthNames: [EALang.january, EALang.february, EALang.march, EALang.april,
                EALang.may, EALang.june, EALang.july, EALang.august, EALang.september,
                EALang.october, EALang.november, EALang.december],
            prevText: EALang.previous,
            nextText: EALang.next,
            currentText: EALang.now,
            closeText: EALang.close,
            timeOnlyTitle: EALang.select_time,
            timeText: EALang.time,
            hourText: EALang.hour,
            minuteText: EALang.minutes,
            firstDay: 0
        });

        var start_time = '{{start_time}}';
        console.log(start_time);
        $('#start-datetime-{{id}}').datetimepicker('setDate', new Date(start_time));

        $dialog.find('#end-datetime-{{id}}').datetimepicker({
            dateFormat: dateFormat,
            timeFormat: 'h:mm TT',

            // Translation
            dayNames: [EALang.sunday, EALang.monday, EALang.tuesday, EALang.wednesday,
                EALang.thursday, EALang.friday, EALang.saturday],
            dayNamesShort: [EALang.sunday.substr(0, 3), EALang.monday.substr(0, 3),
                EALang.tuesday.substr(0, 3), EALang.wednesday.substr(0, 3),
                EALang.thursday.substr(0, 3), EALang.friday.substr(0, 3),
                EALang.saturday.substr(0, 3)],
            dayNamesMin: [EALang.sunday.substr(0, 2), EALang.monday.substr(0, 2),
                EALang.tuesday.substr(0, 2), EALang.wednesday.substr(0, 2),
                EALang.thursday.substr(0, 2), EALang.friday.substr(0, 2),
                EALang.saturday.substr(0, 2)],
            monthNames: [EALang.january, EALang.february, EALang.march, EALang.april,
                EALang.may, EALang.june, EALang.july, EALang.august, EALang.september,
                EALang.october, EALang.november, EALang.december],
            prevText: EALang.previous,
            nextText: EALang.next,
            currentText: EALang.now,
            closeText: EALang.close,
            timeOnlyTitle: EALang.select_time,
            timeText: EALang.time,
            hourText: EALang.hour,
            minuteText: EALang.minutes,
            firstDay: 0
        });

        // add events
        $('#select-provider-{{id}}').change(function () {
            var pid = $('#select-provider-{{id}}').val();

            $('#select-service-{{id}}').empty();

            // get the provider
            var provider = GlobalVariables.availableProviders.find(function(element) {
                return element.id == pid;
            });

            if(typeof provider === 'undefined'){
                alert("Provider do not exist!");
                return;
            }

            var services = GlobalVariables.availableServices.filter(function (service) {
                return provider.services.indexOf(service.id) !== -1;
            });

            console.log(services);

            //add all services

            $.each(services, function (index, service) {
                
                if(index == 0){
                    var optionHtml = '<option value="' + service.id + '" selected="selected">'
                        + service.name + '</option>';

                    var start = $('#start-datetime-{{id}}').datetimepicker('getDate');
                    $('#end-datetime-{{id}}').datetimepicker('setDate', new Date(start.getTime() + service.duration * 60000));

                }else{
                    var optionHtml = '<option value="' + service.id + '">'
                        + service.name + '</option>';
                }

                $('#select-service-{{id}}').append(optionHtml);

            });
        });

        $('#select-service-{{id}}').change(function () {
            var sid = $('#select-service-{{id}}').val();

            // get the provider
            var service = GlobalVariables.availableServices.find(function(element) {
                return element.id == sid;
            });

            var start = $('#start-datetime-{{id}}').datetimepicker('getDate');
            $('#end-datetime-{{id}}').datetimepicker('setDate', new Date(start.getTime() + service.duration * 60000));
        });

        $("#request-button-{{id}}").click(function(e){
            e.preventDefault();
            var value = $("#appointment-request-status-{{id}}").val();
            if(value == 0){
                $("#appointment-request-status-{{id}}").val(1);
                $("#request-button-{{id}}").html('<span class="glyphicon glyphicon-heart"></span>');
            }else{
                $("#appointment-request-status-{{id}}").val(0);
                $("#request-button-{{id}}").html('<span class="glyphicon glyphicon-heart-empty"></span>');
            }
        });


        var is_provider = {{is_provider}};
        if(is_provider){
            var providerId = '{{provider_id}}';
            if(providerId != 'all'){
                $('#select-provider-{{id}} option[value="'
                    + providerId + '"]').prop('selected', true).trigger('change');
                var provider_name = $('#select-provider-{{id}} option[value="' + providerId + '"]').text();
                $('#select-provider-{{id}}').siblings(".ui-autocomplete-input").val(provider_name);
            }
        }



    </script>
</script>


<script id="new-customer-template" type="x-tmpl-mustache">
    <h4>New Customer</h4>
    <div class="row add-new-customer-row">
        <div class="col col-sm-12">
            <!-- Info Here -->
            <div class="form-group">
                <label for="first-name" class="control-label"><?= lang('name') ?> *</label>

                    <input id="first-name"  style="height: 30px;" class="required form-control">

            </div>

            <div class="form-group">
                <label for="last-name" class="control-label"><?= lang('last_name') ?> *</label>
                
                    <input id="last-name" style="height: 30px;" class="required form-control">
                
            </div>

            <div class="form-group">
                <label for="phone-number" class="control-label"><?= lang('phone_number') ?> *</label>

                <input id="phone-number" style="height: 30px;" class="required form-control">
            </div>

            <div class="form-group">
                <label for="email" class="control-label"><?= lang('email') ?></label>
                <input id="email" style="height: 30px;" class="form-control">
            </div>

            <div class="form-group">
                <label for="address" class="control-label"><?= lang('address') ?></label>
                <input id="address" style="height: 30px;" class="form-control">
            </div>

            <div class="form-group">
                <label for="city" class="control-label"><?= lang('city') ?></label>
                <input id="city" style="height: 30px;" class="form-control">
            </div>

            <div class="form-group">
                <label for="customer-notes" class="control-label"><?= lang('notes') ?></label>
                <textarea id="customer-notes" rows="2" class="form-control"></textarea>
            </div>
        </div>
    </div>

</script>