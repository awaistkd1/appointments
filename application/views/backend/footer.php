<div id="footer">
    <div id="footer-content" class="col-xs-12 col-sm-6">
        Powered by
        <a href="http://gaia-beauty.com">Gaia Beauty
            </a> |
        <span id="select-language" class="label label-success">
        	<?= ucfirst($this->config->item('language')) ?>
        </span>
    </div>

    <div id="footer-user-display-name" class="col-xs-12 col-sm-6">
        <?= lang('hello') . ', ' . $user_display_name ?>!
    </div>
</div>

<div id="custom-axis-template" style="display: none;">
    <table class="custom-axis">
    <tbody>
        <tr data-time="00:00:00" >
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span>12 AM</span></td>
        </tr>
		
		 <tr data-time="00:15:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">12 AM</span></td>
        </tr>
        <tr data-time="00:30:00" class="fc-minor">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span >12:30 AM</span></td>
    
        </tr>
		 <tr data-time="00:45:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">12 AM</span></td>
        </tr>
        <tr data-time="01:00:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span>1 AM</span></td>
    
        </tr>
		<tr data-time="01:15:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">1 AM</span></td>
    
        </tr>
        <tr data-time="01:30:00" class="fc-minor">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span>1:30 AM</span></td>
    
        </tr>
		<tr data-time="01:45:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">1 AM</span></td>
    
        </tr>
        <tr data-time="02:00:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span>2 AM</span></td>
    
        </tr>
		<tr data-time="02:15:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">2 AM</span></td>
    
        </tr>
        <tr data-time="02:30:00" class="fc-minor">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span >2:30 AM</span></td>
    
        </tr>
		<tr data-time="02:45:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">2 AM</span></td>
    
        </tr>
        <tr data-time="03:00:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span>3 AM</span></td>
    
        </tr>
		 <tr data-time="03:15:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">3 AM</span></td>
    
        </tr>
        <tr data-time="03:30:00" class="fc-minor">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span>3:30 AM</span></td>
    
        </tr>
		 <tr data-time="03:45:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">3 AM</span></td>
    
        </tr>
        <tr data-time="04:00:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span>4 AM</span></td>
    
        </tr>
		<tr data-time="04:15:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">4 AM</span></td>
    
        </tr>
        <tr data-time="04:30:00" class="fc-minor">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span>4:30 AM</span></td>
    
        </tr>
		<tr data-time="04:45:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">4 AM</span></td>
    
        </tr>
        <tr data-time="05:00:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span>5 AM</span></td>
    
        </tr>
		<tr data-time="05:15:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">5 AM</span></td>
    
        </tr>
        <tr data-time="05:30:00" class="fc-minor">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span>5:30 AM</span></td>
    
        </tr>
		<tr data-time="05:45:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">5 AM</span></td>
    
        </tr>
        <tr data-time="06:00:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span>6 AM</span></td>
    
        </tr>
		  <tr data-time="06:15:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">6 AM</span></td>
    
        </tr>
        <tr data-time="06:30:00" class="fc-minor">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span>6:30 AM</span></td>
    
        </tr>
		  <tr data-time="06:45:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">6 AM</span></td>
    
        </tr>
        <tr data-time="07:00:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span>7 AM</span></td>
    
        </tr>
		 <tr data-time="07:15:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">7 AM</span></td>
    
        </tr>
        <tr data-time="07:30:00" class="fc-minor">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span>7:30 AM</span></td>
    
        </tr>
		 <tr data-time="07:45:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">7 AM</span></td>
    
        </tr>
        <tr data-time="08:00:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span>8 AM</span></td>
    
        </tr>
		 <tr data-time="08:15:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">8 AM</span></td>
    
        </tr>
        <tr data-time="08:30:00" class="fc-minor">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span>8:30 AM</span></td>
    
        </tr>
		 <tr data-time="08:45:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">8 AM</span></td>
    
        </tr>
        <tr data-time="09:00:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span>9 AM</span></td>
    
        </tr>
		 <tr data-time="09:15:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;" >9 AM</span></td>
    
        </tr>
        <tr data-time="09:30:00" class="fc-minor">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span>9:30 AM</span></td>
    
        </tr>
		 <tr data-time="09:45:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">9 AM</span></td>
    
        </tr>
        <tr data-time="10:00:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span>10 AM</span></td>
    
        </tr>
		 <tr data-time="10:15:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">10 AM</span></td>
    
        </tr>
        <tr data-time="10:30:00" class="fc-minor">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span>10:30 AM</span></td>
    
        </tr>
		 <tr data-time="10:45:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">10 AM</span></td>
    
        </tr>
        <tr data-time="11:00:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span>11 AM</span></td>
    
        </tr>
		<tr data-time="11:15:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">11 AM</span></td>
    
        </tr>
        <tr data-time="11:30:00" class="fc-minor">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span >11:30 AM</span></td>
    
        </tr>
		<tr data-time="11:45:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">11 AM</span></td>
    
        </tr>
        <tr data-time="12:00:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span>12 PM</span></td>
    
        </tr>
		  <tr data-time="12:15:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">12 PM</span></td>
    
        </tr>
        <tr data-time="12:30:00" class="fc-minor">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span >12:30 AM</span></td>
    
        </tr>
		  <tr data-time="12:45:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">12 PM</span></td>
    
        </tr>
        <tr data-time="13:00:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span>1 PM</span></td>
    
        </tr>
		 <tr data-time="13:15:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">1 PM</span></td>
    
        </tr>
        <tr data-time="13:30:00" class="fc-minor">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span >1:30 AM</span></td>
    
        </tr>
		 <tr data-time="13:45:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">1 PM</span></td>
    
        </tr>
        <tr data-time="14:00:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span>2 PM</span></td>
    
        </tr>
		 <tr data-time="14:15:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">2 PM</span></td>
    
        </tr>
        <tr data-time="14:30:00" class="fc-minor">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span >2:30 AM</span></td>
    
        </tr>
		 <tr data-time="14:45:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">2 PM</span></td>
    
        </tr>
        <tr data-time="15:00:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span>3 PM</span></td>
    
        </tr>
		<tr data-time="15:15:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">3 PM</span></td>
    
        </tr>
        <tr data-time="15:30:00" class="fc-minor">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span>3:30 AM</span></td>
    
        </tr>
		<tr data-time="15:45:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">3 PM</span></td>
    
        </tr>
        <tr data-time="16:00:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span>4 PM</span></td>
    
        </tr>
		 <tr data-time="16:15:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">4 PM</span></td>
    
        </tr>
        <tr data-time="16:30:00" class="fc-minor">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span >4:30 AM</span></td>
    
        </tr>
		 <tr data-time="16:45:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">4 PM</span></td>
    
        </tr>
        <tr data-time="17:00:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span>5 PM</span></td>
    
        </tr>
		  <tr data-time="17:15:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">5 PM</span></td>
    
        </tr>
        <tr data-time="17:30:00" class="fc-minor">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span >5:30 AM</span></td>
    
        </tr>
		  <tr data-time="17:45:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">5 PM</span></td>
    
        </tr>
        <tr data-time="18:00:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span>6 PM</span></td>
    
        </tr>
		<tr data-time="18:15:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">6 PM</span></td>
    
        </tr>
        <tr data-time="18:30:00" class="fc-minor">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span >6:30 AM</span></td>
    
        </tr>
		<tr data-time="18:45:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">6 PM</span></td>
    
        </tr>
        <tr data-time="19:00:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span>7 PM</span></td>
    
        </tr>
		<tr data-time="19:15:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">7 PM</span></td>
    
        </tr>
        <tr data-time="19:30:00" class="fc-minor">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span >7:30 AM</span></td>
    
        </tr>
		<tr data-time="19:45:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">7 PM</span></td>
    
        </tr>
        <tr data-time="20:00:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span>8 PM</span></td>
    
        </tr>
		 <tr data-time="20:15:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">8 PM</span></td>
    
        </tr>
        <tr data-time="20:30:00" class="fc-minor">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span >8:30 AM</span></td>
    
        </tr>
		 <tr data-time="20:45:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">8 PM</span></td>
    
        </tr>
        <tr data-time="21:00:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span>9 PM</span></td>
    
        </tr>
		<tr data-time="21:15:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">9 PM</span></td>
    
        </tr>
        <tr data-time="21:30:00" class="fc-minor">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span >9:30 AM</span></td>
    
        </tr>
		<tr data-time="21:45:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">9 PM</span></td>
    
        </tr>
        <tr data-time="22:00:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span>10 PM</span></td>
    
        </tr>
		     <tr data-time="22:15:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">10 PM</span></td>
    
        </tr>
        <tr data-time="22:30:00" class="fc-minor">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span >10:30 AM</span></td>
    
        </tr>
		     <tr data-time="22:45:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">10 PM</span></td>
    
        </tr>
        <tr data-time="23:00:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span>11 PM</span></td>
    
        </tr>
		<tr data-time="23:15:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">11 PM</span></td>
    
        </tr>
        <tr data-time="23:30:00" class="fc-minor">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span >11:30 AM</span></td>
    
        </tr>
		<tr data-time="23:45:00">
            <td class="fc-axis fc-time fc-widget-content" style="width: 40.7px;"><span style="visibility: hidden;">11 PM</span></td>
    
        </tr>
    </tbody>
</table>
</div>

<script src="<?= asset_url('assets/js/backend.js') ?>"></script>
<script src="<?= asset_url('assets/js/general_functions.js') ?>"></script>
</body>
</html>
