<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* ----------------------------------------------------------------------------
 * Easy!Appointments - Open Source Web Scheduler
 *
 * @package     EasyAppointments
 * @author      A.Tselegidis <alextselegidis@gmail.com>
 * @copyright   Copyright (c) 2013 - 2017, Alex Tselegidis
 * @license     http://opensource.org/licenses/GPL-3.0 - GPLv3
 * @link        http://easyappointments.org
 * @since       v1.0.0
 * ---------------------------------------------------------------------------- */

class ProviderCategories_Model extends CI_Model
{
    public function add($provider_category)
    {
        $this->validate($provider_category);

        if (!isset($provider_category['id'])) {
            $provider_category['id'] = $this->_insert($provider_category);
        } else {
            $this->_update($provider_category);
        }

        return $provider_category['id'];
    }

    protected function _insert($provider_category)
    {
        if (!$this->db->insert('ea_provider_categories', $provider_category)) {
            throw new Exception('Could not insert provider category to the database.');
        }

        return (int)$this->db->insert_id();
    }

    protected function _update($provider_category)
    {
        if (!$this->db->update('ea_provider_categories', $provider_category, ['id' => $provider_category['id']])) {
            throw new Exception('Could not update provider category to the database.');
        }

        return (int)$provider_category['id'];
    }

    public function validate($provider_category)
    {
        try {
            // Required Fields
            if (!isset($provider_category['name'])) {
                throw new Exception('Not all required fields where provided ');
            }

            if ($provider_category['name'] == '' || $provider_category['name'] == null) {
                throw new Exception('Required fields cannot be empty or null ($provider_category: '
                    . print_r($provider_category, true) . ')');
            }

            return true;
        } catch (Exception $exc) {
            return false;
        }
    }

    public function delete($provider_category_id)
    {
        if (!is_numeric($provider_category_id)) {
            throw new Exception('Invalid argument type $provider_category_id: ' . $provider_category_id);
        }

        $num_rows = $this->db->get_where('ea_provider_categories', ['id' => $provider_category_id])->num_rows();

        if ($num_rows == 0) {
            return false;
        }

        return $this->db->delete('ea_provider_categories', ['id' => $provider_category_id]);
    }

    public function get_row($provider_category_id)
    {
        if (!is_numeric($provider_category_id)) {
            throw new Exception('Invalid argument provided as $provider_category_id : ' . $provider_category_id);
        }
        return $this->db->get_where('ea_provider_categories', ['id' => $provider_category_id])->row_array();
    }

    public function get_batch($where_clause = '')
    {
        if ($where_clause != '') {
            $this->db->where($where_clause);
        }

        return $this->db->get('ea_provider_categories')->result_array();
    }
}
