<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Provider Category Model
 *
 * @package Models
 */

class ProviderCategory_Model extends CI_Model {	

	public function getAll(){
		$query = $this->db->query("SELECT * from ea_provider_categories");
   		return $query->result();
	}

	public function add($provider_category_name){
		// check if already exists or not

		$num_rows = $this->db->get_where('ea_provider_categories', ['name' => $provider_category_name ])->num_rows();
        if ($num_rows != 0)
        {
            throw new Exception('Category already exists.');
        }

        $this->db->insert('ea_provider_categories', ['name'=>$provider_category_name]);

        $category = array();
        $category['name'] = $provider_category_name;
        $category['id'] = $this->db->insert_id();

        return $category;
	}

	public function update($category){
		$this->db->where('id', $category['id']);
        return $this->db->update('ea_provider_categories', $category);
	}


	public function delete($id){
        
        if ( ! is_numeric($id))
        {
            throw new Exception('Invalid argument given for $category_id: ' . $id);
        }


        $num_rows = $this->db->get_where('ea_provider_categories', ['id' => $id])
            ->num_rows();
        if ($num_rows == 0)
        {
            throw new Exception('Provider category record not found in database.');
        }

        $this->db->where('id', $id);

        return $this->db->delete('ea_provider_categories');
	}
}