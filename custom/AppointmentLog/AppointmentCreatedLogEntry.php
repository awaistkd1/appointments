<?php

/* ----------------------------------------------------------------------------
 * Easy!Appointments - Open Source Web Scheduler
 *
 * @package     EasyAppointments
 * @author      A.Tselegidis <alextselegidis@gmail.com>
 * @copyright   Copyright (c) 2013 - 2017, Alex Tselegidis
 * @license     http://opensource.org/licenses/GPL-3.0 - GPLv3
 * @link        http://easyappointments.org
 * @since       v1.2.0
 * ---------------------------------------------------------------------------- */

namespace EA\Custom\AppointmentLog;


/**
 * Class AppointmentCreatedLogEntry
 * @package EA\Custom\AppointmentLog
 */
class AppointmentCreatedLogEntry
{
    /**
     * @var array
     */
    private $appointment;

    /**
     * @var \CI_Model
     */
    private $framework;

    /**
     * AppointmentCreatedLogEntry constructor.
     * @param \CI_Model $framework
     * @param array $appointment
     */
    public function __construct(\CI_Model $framework, array $appointment)
    {
        $this->framework = $framework;
        $this->appointment = $appointment;
    }

    public function persist()
    {
        $userId = !empty($this->framework->session->user_id) ? $this->framework->session->user_id : $this->appointment['id_users_customer'];

        $user = $this->framework->db->get_where('ea_users', ['id' => $userId])->row_array();

        $appointmentLog = [
            'type' => 'insert',
            'user' => $user['first_name'] . ' ' . $user['last_name'],
            'id_appointments' => $this->appointment['id']
        ];

        $this->framework->db->insert('ea_appointment_logs', $appointmentLog);
    }
}