<?php

/* ----------------------------------------------------------------------------
 * Easy!Appointments - Open Source Web Scheduler
 *
 * @package     EasyAppointments
 * @author      A.Tselegidis <alextselegidis@gmail.com>
 * @copyright   Copyright (c) 2013 - 2017, Alex Tselegidis
 * @license     http://opensource.org/licenses/GPL-3.0 - GPLv3
 * @link        http://easyappointments.org
 * @since       v1.2.0
 * ---------------------------------------------------------------------------- */

namespace EA\Custom\AppointmentLog;


use EA\Custom\AppointmentStatus\MapString;

class AppointmentUpdatedLogEntry
{
    /**
     * @var \CI_Model
     */
    private $framework;

    /**
     * @var array
     */
    private $appointment;

    /**
     * @var array
     */
    private $originalAppointment;

    /**
     * AppointmentUpdatedLogEntry constructor.
     * @param \CI_Model $framework
     * @param array $appointment
     */
    public function __construct(\CI_Model $framework, array $appointment)
    {
        $this->framework = $framework;
        $this->appointment = $appointment;
        $this->originalAppointment = $this->framework->db->get_where('ea_appointments', ['id' => $this->appointment['id']])->row_array();
    }

    public function persist()
    {
        $userId = !empty($this->framework->session->user_id) ? $this->framework->session->user_id : $this->appointment['id_users_customer'];

        $user = $this->framework->db->get_where('ea_users', ['id' => $userId])->row_array();

        $changes = [];

        // service
        if ($this->originalAppointment['id_services'] !== $this->appointment['id_services']) {
            $service = $this->framework->db->get_where('ea_services', ['id' => $this->appointment['id_services']])->row_array();
            $originalService = $this->framework->db->get_where('ea_services', ['id' => $this->originalAppointment['id_services']])->row_array();
            $changes[] = [
                'type' => 'update',
                'user' => $user['first_name'] . ' ' . $user['last_name'],
                'field' => 'service',
                'value' => $service['name'] . ' (' . lang('before') . ' ' . $originalService['name'] . ')',
                'id_appointments' => $this->appointment['id']
            ];
        }

        // provider
        if ($this->originalAppointment['id_users_provider'] !== $this->appointment['id_users_provider']) {
            $provider = $this->framework->db->get_where('ea_users', ['id' => $this->appointment['id_users_provider']])->row_array();
            $originalProvider = $this->framework->db->get_where('ea_users', ['id' => $this->originalAppointment['id_users_provider']])->row_array();

            $changes[] = [
                'type' => 'update',
                'user' => $user['first_name'] . ' ' . $user['last_name'],
                'field' => 'provider',
                'value' =>$provider['first_name'] . ' ' . $provider['last_name'] . ' (' . lang('before') . ' ' . $originalProvider['first_name'] . '  ' . $originalProvider['last_name'] . ')',
                'id_appointments' => $this->appointment['id']
            ];
        }

        // start
        if ($this->originalAppointment['start_datetime'] !== $this->appointment['start_datetime']) {
            $changes[] = [
                'type' => 'update',
                'user' => $user['first_name'] . ' ' . $user['last_name'],
                'field' => 'start',
                'value' => $this->appointment['start_datetime'] . ' (' . lang('before') . ' ' . $this->originalAppointment['start_datetime'] . ')',
                'id_appointments' => $this->appointment['id']
            ];
        }

        // end
        if ($this->originalAppointment['end_datetime'] !== $this->appointment['end_datetime']) {
            $changes[] = [
                'type' => 'update',
                'user' => $user['first_name'] . ' ' . $user['last_name'],
                'field' => 'end',
                'value' => $this->appointment['end_datetime'] . ' (' . lang('before') . ' ' . $this->originalAppointment['end_datetime'] . ')',
                'id_appointments' => $this->appointment['id']
            ];
        }

        // notes
        if ($this->originalAppointment['notes'] !== $this->appointment['notes']) {
            $changes[] = [
                'type' => 'update',
                'user' => $user['first_name'] . ' ' . $user['last_name'],
                'field' => 'notes',
                'value' => $this->appointment['notes'] . ' (' . lang('before') . ' ' . (!empty($this->originalAppointment['notes']) ? $this->originalAppointment['notes'] : lang('empty')) . ')',
                'id_appointments' => $this->appointment['id']
            ];
        }

        // status
        if ($this->originalAppointment['status'] !== $this->appointment['status']) {
            $changes[] = [
                'type' => 'update',
                'user' => $user['first_name'] . ' ' . $user['last_name'],
                'field' => 'status',
                'value' => MapString::convert($this->appointment['status']) . ' (' . lang('before') . ' ' . MapString::convert($this->originalAppointment['status']) . ')',
                'id_appointments' => $this->appointment['id']
            ];
        }

        // request_status
        if ($this->originalAppointment['request_status'] !== $this->appointment['request_status']) {
            $changes[] = [
                'type' => 'update',
                'user' => $user['first_name'] . ' ' . $user['last_name'],
                'field' => 'request_status',
                'value' => ($this->appointment['request_status'] ? lang('enabled') : lang('disabled')) . ' (' . lang('before') . ' ' . ($this->originalAppointment['request_status'] ? lang('enabled') : lang('disabled')) . ')',
                'id_appointments' => $this->appointment['id']
            ];
        }

        // confirmation_status
        if ($this->originalAppointment['confirmation_status'] !== $this->appointment['confirmation_status']) {
            $changes[] = [
                'type' => 'update',
                'user' => $user['first_name'] . ' ' . $user['last_name'],
                'field' => 'confirmation_status',
                'value' => MapString::convertConfirmationStatus($this->appointment['confirmation_status']) . ' (' . lang('before') . ' ' . MapString::convertConfirmationStatus($this->originalAppointment['confirmation_status']) . ')',
                'id_appointments' => $this->appointment['id']
            ];
        }

        foreach($changes as $change) {
            $this->framework->db->insert('ea_appointment_logs', $change);
        }
    }
}