<?php


namespace EA\Custom\ColorCoding;


class StatusColorMapping
{
    public function getMapping()
    {
        return [
            '0' => '#4790CA', // booked
            '1' => '#fdd835', // arrive
            '2' => '#ff9800', // complete
            '3' => '#8bc34a', // start
            '4' => '#9e9e9e', // checkout
            '5' => '#f44336' // no-show
        ];
    }
}