<?php


namespace EA\Custom\AppointmentStatus;


class NotConfirmedStatus
{
    const SLUG = 'not-confirmed';
}