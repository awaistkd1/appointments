<?php


namespace EA\Custom\AppointmentStatus;


class MapString
{
    public static function convert($statusId)
    {
        switch($statusId) {
            case 0:
                return lang('booked');
            case 1:
                return lang('arrive');
            case 2:
                return lang('start');
            case 3:
                return lang('complete');
            case 4:
                return lang('checkout');
            case 5:
                return lang('no_show');
        }
    }

    public static function convertConfirmationStatus($statusId)
    {
        switch($statusId) {
            case 0:
                return lang('no_value');
            case 1:
                return lang('confirmed');
            case 2:
                return lang('not_confirmed');
        }
    }
}