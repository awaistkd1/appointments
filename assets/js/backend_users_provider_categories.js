/* ----------------------------------------------------------------------------
 * Easy!Appointments - Open Source Web Scheduler
 *
 * @package     EasyAppointments
 * @author      A.Tselegidis <alextselegidis@gmail.com>
 * @copyright   Copyright (c) 2013 - 2017, Alex Tselegidis
 * @license     http://opensource.org/licenses/GPL-3.0 - GPLv3
 * @link        http://easyappointments.org
 * @since       v1.0.0
 * ---------------------------------------------------------------------------- */

(function () {

    'use strict';

    /**
     * ProviderCategoriesHelper Class
     *
     * This class contains the core method implementations that belong to the provider-categories tab
     * of the backend services page.
     *
     * @class ProviderCategoriesHelper
     */
    function ProviderCategoriesHelper() {
        this.filterResults = {};
    }

    /**
     * Binds the default event handlers of the provider-categories tab.
     */
    ProviderCategoriesHelper.prototype.bindEventHandlers = function () {
        var instance = this;

        /**
         * Event: Filter Categories Cancel Button "Click"
         */
        $('#filter-provider-categories .clear').click(function () {
            $('#filter-provider-categories .key').val('');
            instance.filter('');
            instance.resetForm();
        });

        /**
         * Event: Filter Categories Form "Submit"
         */
        $('#filter-provider-categories form').submit(function () {
            var key = $('#filter-provider-categories .key').val();
            $('.selected').removeClass('selected');
            instance.resetForm();
            instance.filter(key);
            return false;
        });

        /**
         * Event: Filter Categories Row "Click"
         *
         * Displays the selected row data on the right side of the page.
         */
        $(document).on('click', '.provider-category-row', function () {
            if ($('#filter-provider-categories .filter').prop('disabled')) {
                $('#filter-provider-categories .results').css('color', '#AAA');
                return; // exit because we are on edit mode
            }

            var providerCategoryId = $(this).attr('data-id');
            var providerCategory = {};
            $.each(instance.filterResults, function (index, item) {
                if (item.id === providerCategoryId) {
                    providerCategory = item;
                    return false;
                }
            });

            instance.display(providerCategory);
            $('#filter-provider-categories .selected').removeClass('selected');
            $(this).addClass('selected');
            $('#edit-provider-category, #delete-provider-category').prop('disabled', false);
        });

        /**
         * Event: Add Category Button "Click"
         */
        $('#add-provider-category').click(function () {
            instance.resetForm();
            $('#provider-categories .add-edit-delete-group').hide();
            $('#provider-categories .save-cancel-group').show();
            $('#provider-categories .record-details').find('input, textarea').prop('readonly', false);
            $('#filter-provider-categories button').prop('disabled', true);
            $('#filter-provider-categories .results').css('color', '#AAA');
        });

        /**
         * Event: Edit Category Button "Click"
         */
        $('#edit-provider-category').click(function () {
            $('#provider-categories .add-edit-delete-group').hide();
            $('#provider-categories .save-cancel-group').show();
            $('#provider-categories .record-details').find('input, textarea').prop('readonly', false);

            $('#filter-provider-categories button').prop('disabled', true);
            $('#filter-provider-categories .results').css('color', '#AAA');
        });

        /**
         * Event: Delete Category Button "Click"
         */
        $('#delete-provider-category').click(function () {
            var providerCategoryId = $('#provider-category-id').val();

            var buttons = [
                {
                    text: EALang.delete,
                    click: function () {
                        instance.delete(providerCategoryId);
                        $('#message_box').dialog('close');
                    }
                },
                {
                    text: EALang.cancel,
                    click: function () {
                        $('#message_box').dialog('close');
                    }
                }
            ];

            GeneralFunctions.displayMessageBox(EALang.delete_category,
                EALang.delete_record_prompt, buttons);
        });

        /**
         * Event: Categories Save Button "Click"
         */
        $('#save-provider-category').click(function () {
            var providerCategory = {
                name: $('#provider-category-name').val(),
                description: $('#provider-category-description').val()
            };

            if ($('#provider-category-id').val() !== '') {
                providerCategory.id = $('#provider-category-id').val();
            }

            if (!instance.validate()) {
                return;
            }

            instance.save(providerCategory);
        });

        /**
         * Event: Cancel Category Button "Click"
         */
        $('#cancel-provider-category').click(function () {
            var id = $('#provider-category-id').val();
            instance.resetForm();
            if (id !== '') {
                instance.select(id, true);
            }
        });
    };

    /**
     * Filter service provider-categories records.
     *
     * @param {String} key This key string is used to filter the category records.
     * @param {Number} selectId Optional, if set then after the filter operation the record with the given
     * ID will be selected (but not displayed).
     * @param {Boolean} display Optional (false), if true then the selected record will be displayed on the form.
     */
    ProviderCategoriesHelper.prototype.filter = function (key, selectId, display) {
        var postUrl = GlobalVariables.baseUrl + '/index.php/backend_api/ajax_filter_provider_categories';
        var postData = {
            csrfToken: GlobalVariables.csrfToken,
            key: key
        };

        $.post(postUrl, postData, function (response) {
            if (!GeneralFunctions.handleAjaxExceptions(response)) {
                return;
            }

            this.filterResults = response;

            $('#filter-provider-categories .results').html('');
            $.each(response, function (index, category) {
                var html = this.getFilterHtml(category);
                $('#filter-provider-categories .results').append(html);
            }.bind(this));

            if (response.length === 0) {
                $('#filter-provider-categories .results').html('<em>' + EALang.no_records_found + '</em>');
            }

            if (selectId !== undefined) {
                this.select(selectId, display);
            }
        }.bind(this), 'json').fail(GeneralFunctions.ajaxFailureHandler);
    };

    /**
     * Save a category record to the database (via AJAX post).
     *
     * @param {Object} category Contains the category data.
     */
    ProviderCategoriesHelper.prototype.save = function (category) {
        var postUrl = GlobalVariables.baseUrl + '/index.php/backend_api/ajax_save_provider_category';
        var postData = {
            csrfToken: GlobalVariables.csrfToken,
            provider_category: JSON.stringify(category)
        };

        $.post(postUrl, postData, function (response) {
            if (!GeneralFunctions.handleAjaxExceptions(response)) {
                return;
            }

            Backend.displayNotification(EALang.provider_category_saved);
            this.resetForm();
            $('#filter-provider-categories .key').val('');
            this.filter('', response.id, true);
        }.bind(this), 'json').fail(GeneralFunctions.ajaxFailureHandler);
    };

    /**
     * Delete category record.
     *
     * @param Number} id Record ID to be deleted.
     */
    ProviderCategoriesHelper.prototype.delete = function (id) {
        var postUrl = GlobalVariables.baseUrl + '/index.php/backend_api/ajax_delete_provider_category';
        var postData = {
            csrfToken: GlobalVariables.csrfToken,
            provider_category_id: id
        };

        $.post(postUrl, postData, function (response) {
            if (!GeneralFunctions.handleAjaxExceptions(response)) {
                return;
            }

            Backend.displayNotification(EALang.provider_category_deleted);

            this.resetForm();
            this.filter($('#filter-provider-categories .key').val());
        }.bind(this), 'json').fail(GeneralFunctions.ajaxFailureHandler);
    };

    /**
     * Display a category record on the form.
     *
     * @param {Object} category Contains the category data.
     */
    ProviderCategoriesHelper.prototype.display = function (category) {
        $('#provider-category-id').val(category.id);
        $('#provider-category-name').val(category.name);
        $('#provider-category-description').val(category.description);
    };

    /**
     * Validate category data before save (insert or update).
     *
     * @return {Boolean} Returns the validation result.
     */
    ProviderCategoriesHelper.prototype.validate = function () {
        $('#provider-categories .has-error').removeClass('has-error');

        try {
            var missingRequired = false;

            $('#provider-categories .required').each(function () {
                if ($(this).val() === '' || $(this).val() === undefined) {
                    $(this).closest('.form-group').addClass('has-error');
                    missingRequired = true;
                }
            });

            if (missingRequired) {
                throw EALang.fields_are_required;
            }

            return true;
        } catch (message) {
            return false;
        }
    };

    /**
     * Bring the category form back to its initial state.
     */
    ProviderCategoriesHelper.prototype.resetForm = function () {
        $('#provider-categories .add-edit-delete-group').show();
        $('#provider-categories .save-cancel-group').hide();
        $('#provider-categories .record-details').find('input, textarea').val('');
        $('#provider-categories .record-details').find('input, textarea').prop('readonly', true);
        $('#edit-category, #delete-category').prop('disabled', true);

        $('#filter-provider-categories .selected').removeClass('selected');
        $('#filter-provider-categories .results').css('color', '');
        $('#filter-provider-categories button').prop('disabled', false);
    };

    /**
     * Get the filter results row HTML code.
     *
     * @param {Object} providerCategory Contains the providerCategory data.
     *
     * @return {String} Returns the record HTML code.
     */
    ProviderCategoriesHelper.prototype.getFilterHtml = function (providerCategory) {
        var html =
            '<div class="provider-category-row entry" data-id="' + providerCategory.id + '">' +
            '<strong>' + providerCategory.name + '</strong>' +
            '</div><hr>';

        return html;
    };

    /**
     * Select a specific record from the current filter results.
     *
     * If the category ID does not exist in the list then no record will be selected.
     *
     * @param {Number} id The record ID to be selected from the filter results.
     * @param {Boolean} display Optional (false), if true then the method will display the record
     * on the form.
     */
    ProviderCategoriesHelper.prototype.select = function (id, display) {
        display = display || false;

        $('#filter-provider-categories .selected').removeClass('selected');

        $('#filter-provider-categories .provider-category-row').each(function () {
            if ($(this).attr('data-id') == id) {
                $(this).addClass('selected');
                return false;
            }
        });

        if (display) {
            $.each(this.filterResults, function (index, providerCategory) {
                if (providerCategory.id == id) {
                    this.display(providerCategory);
                    $('#edit-provider-category, #delete-provider-category').prop('disabled', false);
                    return false;
                }
            }.bind(this));
        }
    };

    window.ProviderCategoriesHelper = ProviderCategoriesHelper;
})();
