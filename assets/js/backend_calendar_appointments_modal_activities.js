/* ----------------------------------------------------------------------------
 * Easy!Appointments - Open Source Web Scheduler
 *
 * @package     EasyAppointments
 * @author      A.Tselegidis <alextselegidis@gmail.com>
 * @copyright   Copyright (c) 2013 - 2017, Alex Tselegidis
 * @license     http://opensource.org/licenses/GPL-3.0 - GPLv3
 * @link        http://easyappointments.org
 * @since       v1.2.0
 * ---------------------------------------------------------------------------- */

/**
 * Backend Calendar Appointments Modal
 *
 * This module implements the appointments modal functionality.
 *
 * @module BackendCalendarAppointmentsModal
 */
window.BackendCalendarAppointmentsModal = window.BackendCalendarAppointmentsModal || {};
window.BackendCalendarAppointmentsModal.Activities = window.BackendCalendarAppointmentsModal.Activities || {};

(function (exports) {

    'use strict';

    var $container = $('.activity-container');

    var displayLog = function (log) {
        $('<div/>', {
            'class': 'well',
            'data': log,
            'html': [
                $('<h6/>', {
                    'text': GeneralFunctions.formatDate(log.created, GlobalVariables.dateFormat, true)
                }),
                $('<p/>', {
                    'text': log.type === 'insert'
                        ? log.user + ' ' + EALang.has_created_the_appointment
                        : log.user + ' ' + EALang.has_changed + ' ' + EALang[log.field] + ' ' + EALang.to + ' ' + log.value
                })
            ]
        })
            .appendTo('.activity-container');
    };

    exports.display = function (logs) {
        $container.empty();
        logs.forEach(displayLog)
    };

})(window.BackendCalendarAppointmentsModal.Activities);
