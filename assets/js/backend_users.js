/* ----------------------------------------------------------------------------
 * Easy!Appointments - Open Source Web Scheduler
 *
 * @package     EasyAppointments
 * @author      A.Tselegidis <alextselegidis@gmail.com>
 * @copyright   Copyright (c) 2013 - 2017, Alex Tselegidis
 * @license     http://opensource.org/licenses/GPL-3.0 - GPLv3
 * @link        http://easyappointments.org
 * @since       v1.0.0
 * ---------------------------------------------------------------------------- */

window.BackendUsers = window.BackendUsers || {};

/**
 * Backend Users
 *
 * This module handles the js functionality of the users backend page. It uses three other
 * classes (defined below) in order to handle the admin, provider and secretary record types.
 *
 * @module BackendUsers
 */
(function (exports) {

    'use strict';

    /**
     * Minimum Password Length
     *
     * @type {Number}
     */
    exports.MIN_PASSWORD_LENGTH = 7;

    /**
     * Contains the current tab record methods for the page.
     *
     * @type {AdminsHelper|ProvidersHelper|SecretariesHelper}
     */
    var helper = {};

    /**
     * Use this class instance for performing actions on the working plan.
     *
     * @type {WorkingPlan}
     */
    exports.wp = {};

    /**
     * Initialize the backend users page.
     *
     * @param {Boolean} defaultEventHandlers (OPTIONAL) Whether to bind the default event handlers.
     */
    exports.initialize = function (defaultEventHandlers) {
        defaultEventHandlers = defaultEventHandlers || true;

        exports.wp = new WorkingPlan();
        exports.wp.bindEventHandlers();

        // Instantiate default helper object (admin).
        helper = new ProvidersHelper();
        helper.resetForm();
        helper.filter('');
        helper.bindEventHandlers();

        // Fill the services and providers list boxes.
        var $providerServices = $('#provider-services');

        $providerServices.empty();

        var serviceCategories = {
            'null': {
                name: EALang.uncategorized,
                services: []
            }
        };

        GlobalVariables.serviceCategories.forEach(function (serviceCategory) {
            serviceCategories[serviceCategory.id] = {
                name: serviceCategory.name,
                services: []
            }
        });

        GlobalVariables.services.forEach(function (service) {
            if (!service.id_service_categories) {
                serviceCategories['null'].services.push(service);
            } else {
                serviceCategories[service.id_service_categories].services.push(service);
            }
        });

        for (var id in serviceCategories) {
            var serviceCategory = serviceCategories[id];

            var html = [
                $('<h6/>', {
                    'html': [
                        $('<input/>', {
                            'type':'checkbox',
                            'data-id': id,
                            'class': 'collapse-checkbox',
                            'on': {
                                'click': function () {
                                    $('[data-category-id="' + $(this).attr('data-id') + '"] input:checkbox').prop('checked', $(this).prop('checked'));
                                }
                            }
                        }),

                        $('<span/>', {
                            'text': serviceCategory.name,
                            'data-id': id,
                            'class': 'collapse-link',
                            'on': {
                                'click': function () {
                                    $('[data-category-id="' + $(this).attr('data-id') + '"]').toggleClass('hidden');
                                }
                            }
                        })
                    ]
                })
            ];

            serviceCategory.services.forEach(function (service) {
                html.push(
                    $('<div/>', {
                        'class': 'checkbox',
                        'html': [
                            $('<label/>', {
                                'class': 'hidden',
                                'data-category-id': id,
                                'html': [
                                    $('<input/>', {
                                        'type': 'checkbox',
                                        'data-id': service.id
                                    }),
                                    $('<span/>', {
                                        'text': service.name
                                    })
                                ]
                            })
                        ]
                    })
                )
            });

            $('<div/>', {
                'data-id': id,
                'html': html
            })
                .appendTo($providerServices);
        }

        var $providerCategoriesList = $('#provider-categories-list');
        var $secretaryProviders = $('#secretary-providers');

        $secretaryProviders.empty();
        $providerCategoriesList.empty();

        var providerCategories = {
            'null': {
                name: EALang.uncategorized,
                providers: []
            }
        };

        GlobalVariables.providerCategories.forEach(function (providerCategory) {
            providerCategories[providerCategory.id] = {
                name: providerCategory.name,
                providers: []
            }
        });

        GlobalVariables.providers.forEach(function (provider) {
            if (!provider.categories.length) {
                providerCategories['null'].providers.push(provider);
            } else {
                provider.categories.forEach(function (categoryId) {
                    providerCategories[categoryId].providers.push(provider);
                });
            }
        });

        for (var id in providerCategories) {
            var providerCategory = providerCategories[id];

            // Providers tab
            if (id !== 'null') {
                $('<div/>', {
                    'class': 'checkbox',
                    'html': [
                        $('<label/>', {
                            'html': [
                                $('<input/>', {
                                    'type': 'checkbox',
                                    'data-id': id
                                }),
                                $('<span/>', {
                                    'text': providerCategory.name
                                })
                            ]
                        })
                    ]
                })
                    .appendTo($providerCategoriesList);
            }

            // Secretaries tab

            var html = [
                $('<h6/>', {
                    'text': providerCategory.name
                })
            ];

            providerCategory.providers.forEach(function (provider) {
                html.push(
                    $('<div/>', {
                        'class': 'checkbox',
                        'html': [
                            $('<label/>', {
                                'html': [
                                    $('<input/>', {
                                        'type': 'checkbox',
                                        'data-id': provider.id
                                    }),
                                    $('<span/>', {
                                        'text': provider.first_name + ' ' + provider.last_name
                                    })
                                ]
                            })
                        ]
                    })
                )
            });

            $('<div/>', {
                'data-id': id,
                'html': html
            })
                .appendTo($secretaryProviders);
        }

        $('#reset-working-plan').qtip({
            position: {
                my: 'top center',
                at: 'bottom center'
            },
            style: {
                classes: 'qtip-green qtip-shadow custom-qtip'
            }
        });





        // Bind event handlers.
        if (defaultEventHandlers) {
            _bindEventHandlers();
        }
    };

    /**
     * Binds the default backend users event handlers. Do not use this method on a different
     * page because it needs the backend users page DOM.
     */
    function _bindEventHandlers() {
        /**
         * Event: Page Tab Button "Click"
         *
         * Changes the displayed tab.
         */
        $('a[data-toggle="tab"]').on('shown.bs.tab', function () {
            if ($(this).attr('href') === '#admins') {
                helper = new AdminsHelper();
            } else if ($(this).attr('href') === '#providers') {
                helper = new ProvidersHelper();
            } else if ($(this).attr('href') === '#provider-categories') {
                helper = new ProviderCategoriesHelper();
            } else if ($(this).attr('href') === '#secretaries') {
                helper = new SecretariesHelper();
            }

            helper.resetForm();
            helper.filter('');
            helper.bindEventHandlers();
            $('.filter-key').val('');
            Backend.placeFooterToBottom();
        });

        /**
         * Event: Admin, Provider, Secretary Username "Focusout"
         *
         * When the user leaves the username input field we will need to check if the username
         * is not taken by another record in the system. Usernames must be unique.
         */
        $('#admin-username, #provider-username, #secretary-username').focusout(function () {
            var $input = $(this);

            if ($input.prop('readonly') == true || $input.val() == '') {
                return;
            }

            var userId = $input.parents().eq(2).find('.record-id').val();

            if (userId == undefined) {
                return;
            }

            var postUrl = GlobalVariables.baseUrl + '/index.php/backend_api/ajax_validate_username';
            var postData = {
                csrfToken: GlobalVariables.csrfToken,
                username: $input.val(),
                user_id: userId
            };

            $.post(postUrl, postData, function (response) {
                if (!GeneralFunctions.handleAjaxExceptions(response)) {
                    return;
                }

                if (response == false) {
                    $input.closest('.form-group').addClass('has-error');
                    $input.attr('already-exists', 'true');
                    $input.parents().eq(3).find('.form-message').text(EALang.username_already_exists);
                    $input.parents().eq(3).find('.form-message').show();
                } else {
                    $input.closest('.form-group').removeClass('has-error');
                    $input.attr('already-exists', 'false');
                    if ($input.parents().eq(3).find('.form-message').text() == EALang.username_already_exists) {
                        $input.parents().eq(3).find('.form-message').hide();
                    }
                }
            }, 'json').fail(GeneralFunctions.ajaxFailureHandler);
        });
    }

})(window.BackendUsers);
